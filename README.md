# Building AutoSD: What is Pipe-X?

We wanted to build a software supply chain for
[AutoSD](https://sigs.centos.org/automotive/) that would allow us to extend the
Centos Stream distribution with customizations to the packages and inject
additional business logic into the delivery pipeline. Essentially, we wanted to
build AutoSD as a child distro from the Centos Stream 9 parent distro in a way
that wouldn’t disrupt the parent distro delivery pipelines. 

We imagined this child distro would inherit the majority of its packages from
the parent (as is, without rebuilding) but not the full set. It would also
include additional packages that not available from the parent or override the
package version from the parent.  We wanted to not be locked in to the
environment where these packages are built and to be adaptable and flexible
about this (although we currently only need to work with Koji as an RPM build
service).

# Introducing Pipe-X

Pipe-X is a workflow system for building a child distro from a parent distro
with customizations to the package set and a delivery pipeline that integrates
custom business logic at different stages of the process. It’s built on top of
GitLab CI, which has the advantages of a powerful CI definition language and
containerized job environments. By using GitLab CI, we also benefit from the
ability to inherit and extend any pipeline definitions in downstream instances,
allowing us to spawn completely separate instances of Pipe-X to build other
distros.

The core of Pipe-X is a JSON file stored in a git repository called the
[`lockfile`](https://gitlab.com/CentOS/automotive/sample-images/-/blob/main/package_list/cs9-image-manifest.lock.json),
which defines the packages and associated versions that make up the distro.
Pipe-X uses this file to apply git-ops workflows to track and update the product
definition of the distro. By using git and having the file be accessible as a
plain text file, anyone with basic git experience can understand and interact
with the product definition. It also acts as an abstraction layer, separating
the side of the process that manages the product definition from the side that
assembles and tests changes to the product definition. A major benefit of having
this abstraction is that it supports hot-swapping the product definition
workflow with different workflows without affecting the assembly and test side. 

![Simplified Pipe-X Architecture](pipe-x.png "Simplified Pipe-X Architecture")

We generate and update the `lockfile` using a program named `make-lockfile`.
This program runs as a scheduled job in GitLab CI, like a cronjob. It takes the
definition of which packages to add to the `lockfile` from a service named
[Content Resolver](https://tiny.distro.builders/view--automotive-view.html) and
determines the newest versions of these packages in the Centos Stream compose or
Koji build service to create or update the `lockfile`. It then creates a merge
request in the `lockfile` git repository with this updated `lockfile` and acts
as a trigger for the other side of the process.

On the other side of the process, which is triggered by the merge request from
`make-lockfile`, is a GitLab CI pipeline that takes the `lockfile` as input,
assembles the yum repository, and performs tests on the changes. These tests can
include checking compatibility against important hardware platforms not
considered by the parent distro or applying checks. Successfully validated
changes are reflected as successful CI pipeline runs on the `lockfile` merge
requests, while broken changes appear as failed CI pipelines. When these changes
are merged, the pipeline runs again to assemble, retest, and make available the
new yum repository and system images to distribution services. 

Pipe-X does not provide any tools to build RPMs for the distro itself, instead
depending on existing external tooling such as Koji or Mock in order to build
RPMs. These RPMs can then be absorbed into the child distro and unified with the
package set coming from the parent distro. This keeps our setup flexible and not
locked into a single source or tooling.

Pipe-X does, however, provide package-level pipelines, which are pipelines that
are triggered on new builds of packages from the RPM build tooling. In our case,
we currently only support Koji builds.  We use a microservice, hosted in
OpenShift, to connect events that we are interested in on the Fedora Message Bus
to our package-level pipelines, triggering jobs with parameters that reflect the
event details, such as the package and build details.

# In Operation

Today, we have this pipeline in operation and [available open
source](https://gitlab.com/redhat/edge/ci-cd/pipe-x). This pipeline is building
and testing AutoSD and making [nightly build
artifacts](https://autosd.sig.centos.org/AutoSD-9/nightly/) available for the
AutoSD community to develop with.

In the future, we hope to continue experimenting with the `lockfile` update
workflows to investigate alternative approaches, add and extend the test suites
and business workflows applied, and experiment with new DevOps techniques to
find ways to improve the efficiency and experience for a distro maintainer using
Pipe-X.

